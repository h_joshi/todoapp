//
//  Constant.swift
//  ToDoApp
//
//  Created by Hardik on 08/06/23.
//

import UIKit

//--------------
// MARK: - Ratio
//--------------
let _screenSize = UIScreen.main.bounds.size
let _heightRatio : CGFloat = {
    let ratio = _screenSize.height / 896
    return ratio
}()

let _widthRatio : CGFloat = {
    let ratio = _screenSize.width / 414
    return ratio
}()

//--------------------
//MARK: - Custom print
//--------------------
func _print(_ items: Any...) {
    #if DEBUG
    for item in items {
        print(item)
    }
    #endif
}

//--------------------
//MARK: - Storyboards
//--------------------
let _mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

//-------------------------------
//MARK: - Other default constants
//-------------------------------
let _userDefault = UserDefaults.standard
let _defaultNoficationCenter = NotificationCenter.default
var _appDelegateInstance: AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}

let _dispatchGroup = DispatchGroup()

//-----------------
//MARK: - Constant
//-----------------
class Constant {
    public static let _NULL_STRING: String = ""
    public static let _NULL_INT: Int = 0
    
    static var arrToDoList : [ToDoListModel]! {
        set {
            guard let unwrappedKey = newValue else {
                return
            }
            if let encoded = try? JSONEncoder().encode(unwrappedKey) {
                _userDefault.set(encoded, forKey: KeyMessages.kToDoList)
                _userDefault.synchronize()
            }
        } get {
            if let userData = _userDefault.data(forKey: KeyMessages.kToDoList),
               let user = try? JSONDecoder().decode([ToDoListModel].self, from: userData) {
                return user
            }
            return nil
        }
    }
    
    static var id: Int {
        set {
            _userDefault.set(newValue, forKey: KeyMessages.kToDoID)
            _userDefault.synchronize()
        }
        get {
            return _userDefault.integer(forKey: KeyMessages.kToDoID)
        }
    }
}

