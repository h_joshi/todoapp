//
//  KeyMessages.swift
//  ToDoApp
//
//  Created by Hardik on 09/06/23.
//

import Foundation

class KeyMessages {
    public static let kToDoList = "ToDoList"
    public static let kToDoID = "ToDoID"
    public static let kInsetTitle = "Please insert title"
    public static let kInserTime = "Please select time"
    public static let kTaskList = "Task List"
    public static let kAddTask = "Add Task"
    public static let kUpdateTask = "Update Task"
    public static let kUpdateButton = "Update"
    public static let kDeleteConfirmation1 = "Do you want to delete"
    public static let kDeleteConfirmation2 = ", this action can’t be undone?"
    public static let kBack = "Back"
    public static let kPleaseSelectDateRange = "Please select date range!"
}
