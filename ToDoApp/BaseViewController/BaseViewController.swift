//
//  BaseViewController.swift
//  ToDoApp
//
//  Created by Hardik on 08/06/23.
//

import UIKit

class BaseViewController: UIViewController {
    //----------------------------
    // MARK: - Outlets Declaration
    //----------------------------
    @IBOutlet var horizontalConstraints: [NSLayoutConstraint]?
    @IBOutlet var verticalConstraints: [NSLayoutConstraint]?
    
    //------------------------------
    // MARK: - Variables Declaration
    //------------------------------
    
    
    //-------------------------------
    // MARK: - View LifeCycle Methods
    //-------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        constraintUpdate()
        //Uncomment below block if you need to stop interactivePopGestureRecognizer
        if navigationController?.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)) ?? false {
            navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            _print("Off interactivePopGestureRecognizer from base view controller")
        }
        navigationController?.setNavigationBarHidden(false, animated: false)
        _print("Allocated: \(self.classForCoder)")
    }
    
    //-----------------------
    // MARK: - Custom Methods
    //-----------------------
    // This will update constaints and shrunk it as device screen goes lower.
    func constraintUpdate() {
        if let hConts = horizontalConstraints {
            for const in hConts {
                let v1 = const.constant
                let v2 = v1 * _widthRatio
                const.constant = v2
            }
        }
        if let vConst = verticalConstraints {
            for const in vConst {
                let v1 = const.constant
                let v2 = v1 * _heightRatio
                const.constant = v2
            }
        }
        self.updateLayout()
    }
    
    // This will refresh and update layout if needed.
    func updateLayout() {
        self.view.updateConstraints()
        self.view.layoutIfNeeded()
        self.view.setNeedsLayout()
    }
    
    func setupNavigationTitle(withTitle title: String, forViewController viewController: UIViewController, isRightButtonAdded: Bool = false) {
        if let navigationBar = navigationController?.navigationBar {
            let font = UIFont.robotoFontWith(fontType: .roboto_medium, size: 17.0)
            let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: AppColor.blackColor]
            navigationBar.titleTextAttributes = attributes
        }
        viewController.navigationItem.title = title
        let backButton = UIBarButtonItem()
        backButton.title = KeyMessages.kBack
        viewController.navigationItem.backBarButtonItem = backButton
        
        if isRightButtonAdded {
            let filterButton = UIBarButtonItem(image: UIImage(named: "icn_filter.png"), style: .done, target: self, action: #selector(filterButtonTapped))
            filterButton.tintColor = AppColor.themeColor
            navigationItem.rightBarButtonItems = [filterButton]
        }
    }
        
    @objc func filterButtonTapped() {
        // Handle filter button tap
    }
    
    func showToast(message: String) {
        let toastContainer = UIView(frame: CGRect())
        toastContainer.backgroundColor = UIColor.darkGray //UIColor.black.withAlphaComponent(0.6)
        toastContainer.alpha = 0.0
        toastContainer.layer.cornerRadius = 20
        toastContainer.clipsToBounds  =  true
        
        let toastLabel = UILabel(frame: CGRect())
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font.withSize(30)
        toastLabel.text = message
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 0
        
        toastContainer.addSubview(toastLabel)
        self.view.addSubview(toastContainer)
        
        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastContainer.translatesAutoresizingMaskIntoConstraints = false
        
        let centerX = NSLayoutConstraint(item: toastLabel, attribute: .centerX, relatedBy: .equal, toItem: toastContainer, attribute: .centerXWithinMargins, multiplier: 1, constant: 0)
        let lableBottom = NSLayoutConstraint(item: toastLabel, attribute: .bottom, relatedBy: .equal, toItem: toastContainer, attribute: .bottom, multiplier: 1, constant: -15)
        let lableTop = NSLayoutConstraint(item: toastLabel, attribute: .top, relatedBy: .equal, toItem: toastContainer, attribute: .top, multiplier: 1, constant: 15)
        toastContainer.addConstraints([centerX, lableBottom, lableTop])
        
        let containerCenterX = NSLayoutConstraint(item: toastContainer, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        let containerTrailing = NSLayoutConstraint(item: toastContainer, attribute: .width, relatedBy: .equal, toItem: toastLabel, attribute: .width, multiplier: 1.1, constant: 0)
        let containerBottom = NSLayoutConstraint(item: toastContainer, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -75)
        self.view.addConstraints([containerCenterX,containerTrailing, containerBottom])
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
            toastContainer.alpha = 1.0
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                toastContainer.alpha = 0.0
            }, completion: {_ in
                toastContainer.removeFromSuperview()
            })
        })
    }
    
    func GetLocalStringDateFromUTC(date:Date)-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+5:30") //Current time zone different
        let myString = dateFormatter.string(from: date) //according to date format your date string
        return myString //Convert String to Date
    }
    
    deinit{
        _defaultNoficationCenter.removeObserver(self)
        _print("Deallocated: \(self.classForCoder)")
    }
}
