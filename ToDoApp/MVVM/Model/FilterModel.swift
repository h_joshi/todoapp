//
//  FilterModel.swift
//  ToDoApp
//
//  Created by Hardik on 12/06/23.
//

import Foundation

enum FilterType: String, CaseIterable {
    case today =  "Today"
    case yesterday = "Yesterday"
    case customDate = "Custom Date"
    case sortByAZ = "Sort by A-Z"
    case sortByZA = "Sort by Z-A"
    case clearFilter = "Clear filter"
}
