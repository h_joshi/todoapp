//
//  ToDoListModel.swift
//  ToDoApp
//
//  Created by Hardik on 08/06/23.
//

import Foundation

struct ToDoListModel: Codable {
    var id: Int?
    var taskTitle: String?
    var taskTime: Date?
    var isSelected: Bool?
    
    init(id: Int, taskTitle: String?, taskTime: Date?, isSelected: Bool?) {
        self.id = id
        self.taskTitle = taskTitle
        self.taskTime = taskTime
        self.isSelected = isSelected
    }
    
    init() {
        
    }
}
