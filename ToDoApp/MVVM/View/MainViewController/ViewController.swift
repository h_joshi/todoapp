//
//  ViewController.swift
//  ToDoApp
//
//  Created by Hardik on 08/06/23.
//

import UIKit

class ViewController: BaseViewController {
    //----------------------------
    // MARK: - Outlets Declaration
    //----------------------------
    @IBOutlet private weak var tblTodoList: UITableView!
    @IBOutlet private weak var viewFilterTableHolder: UIView!
    @IBOutlet private weak var tblFilterList: UITableView!
    @IBOutlet private weak var btnPlus: UIButton!
    
    @IBOutlet private weak var imgBlureView: UIImageView!
    @IBOutlet private weak var viewDeletePopup: UIView!
    @IBOutlet private weak var lblDeleteMessage: UILabel!
    @IBOutlet private weak var btnCancle: UIButton!
    @IBOutlet private weak var btnOk: UIButton!
    
    @IBOutlet private weak var viewSlecteDateHolder: UIView!
    @IBOutlet private weak var txtStartDate: UITextField!
    @IBOutlet private weak var txtEndDate: UITextField!
    @IBOutlet private weak var btnCancelDate: UIButton!
    @IBOutlet private weak var btnSelectDate: UIButton!
    
    @IBOutlet private weak var tblTodoViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var tblFilterViewHeightConstraint: NSLayoutConstraint!
    
    //------------------------------
    // MARK: - Variables Declaration
    //------------------------------
    private let objViewModel: ViewModel = ViewModel()
    private var timer: Timer?
    private let startTimePicker = UIDatePicker()
    private let endTimePicker = UIDatePicker()
    
    //-------------------------------
    // MARK: - View LifeCycle Methods
    //-------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        initialDataSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ViewModel.currentFilter = .clearFilter
        objViewModel.filterTaskData()
        tblTodoList.reloadData()
    }
    
    //-----------------------
    // MARK: - Custom Methods
    //-----------------------
    static func instance() -> ViewController {
        _mainStoryboard.instantiateViewController(withIdentifier: ViewController.className) as! ViewController
    }
    
    private func initialDataSetup() {
        setupNavigationTitle(withTitle: KeyMessages.kTaskList, forViewController: self, isRightButtonAdded: true)
        setUpViewModelBindings()
        setupDeletePopupView()
        setupTodoListTableView()
        setupFilterListTableView()
        startTimer()
        setupPickerView()
    }
    
    private func setUpViewModelBindings() {
        self.objViewModel.didFinishRemove = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.objViewModel.filterTaskData()
                self.tblTodoList.reloadData()
            }
        }
        
        self.objViewModel.didRefreshData = { [weak self] in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.tblTodoList.reloadData()
            }
        }
    }
    
    private func setupDeletePopupView() {
        self.viewDeletePopup.alpha = 0
        self.imgBlureView.alpha = 0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        imgBlureView.isUserInteractionEnabled = true
        imgBlureView.addGestureRecognizer(tapGesture)
    }
    
    private func setupPickerView() {
        viewSlecteDateHolder.isHidden = true
        imgBlureView.alpha = 0
        
        Formatter.time.defaultDate = Calendar.current.startOfDay(for: Date())
        startTimePicker.datePickerMode = .dateAndTime
        startTimePicker.locale = Locale(identifier: "en_US")
        txtStartDate.inputView = startTimePicker
        startTimePicker.addTarget(self, action: #selector(handleStartTimePicker(sender:)), for: .valueChanged)
        
        endTimePicker.datePickerMode = .dateAndTime
        endTimePicker.locale = Locale(identifier: "en_US")
        txtEndDate.inputView = endTimePicker
        endTimePicker.addTarget(self, action: #selector(handleEndTimePicker(sender:)), for: .valueChanged)        
    }
    
    private func setupTodoListTableView() {
        tblTodoList.delegate = self
        tblTodoList.dataSource = self
        tblTodoList.register(UINib(nibName: NoteTableViewCell.className, bundle: nil), forCellReuseIdentifier: NoteTableViewCell.className)
        tblTodoList.register(UINib(nibName: NoDataTableViewCell.className, bundle: nil), forCellReuseIdentifier: NoDataTableViewCell.className)
        tblTodoList.layer.borderColor = AppColor.borderColor.cgColor
        tblTodoList.layer.borderWidth = 1.0
        tblTodoList.layer.cornerRadius = 4.0
        tblTodoList.estimatedRowHeight = 70
        tblTodoList.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    private func setupFilterListTableView() {
        tblFilterList.delegate = self
        tblFilterList.dataSource = self
        viewFilterTableHolder.isHidden = true
        tblFilterList.backgroundColor = AppColor.whiteColor
        tblFilterList.estimatedRowHeight = 70
        tblFilterList.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    private func startTimer() {
        timer?.invalidate()
        timer  = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { [weak self] _ in
            self?.tblTodoList.reloadData()
        })
        RunLoop.current.add(timer!, forMode: .common)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize") {
            if let newvalue = change?[.newKey] {
                DispatchQueue.main.async {
                    let newsize  = newvalue as! CGSize
                    if object as? NSObject == self.tblTodoList {
                        self.tblTodoViewHeightConstraint.constant = newsize.height
                    }else if object as? NSObject == self.tblFilterList {
                        self.tblFilterViewHeightConstraint.constant = newsize.height
                    }
                }
            }
        }
    }
    
    //------------------
    // MARK: - IBActions
    //------------------
    
    @objc func handleStartTimePicker(sender: UIDatePicker) {
        txtStartDate.text = GetLocalStringDateFromUTC(date: startTimePicker.date)
        ViewModel.startDate = startTimePicker.date
    }
    
    @objc func handleEndTimePicker(sender: UIDatePicker) {
        txtEndDate.text = GetLocalStringDateFromUTC(date: endTimePicker.date)
        ViewModel.endDate = endTimePicker.date
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        UIView.animate(withDuration: 0.2) {
            self.viewDeletePopup.alpha = 0
            self.imgBlureView.alpha = 0
            self.viewFilterTableHolder.isHidden = true
        }
    }
    
    override func filterButtonTapped() {
        _print("Filter button tapped")
        UIView.animate(withDuration: 0.2) {
            self.viewFilterTableHolder.isHidden = !self.viewFilterTableHolder.isHidden
            self.imgBlureView.alpha = 0.8
            self.tblFilterList.reloadData()
        }
    }
    
    @IBAction private func btnPlusAction(_ sender: UIButton) {
        let addToDoView = AddToDoViewController.instance()
        self.navigationController?.pushViewController(addToDoView, animated: true)
    }
    
    @IBAction private func btnCancelAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2, delay: 0.2) {
            self.viewDeletePopup.alpha = 0
            self.imgBlureView.alpha = 0
        }
    }
    
    @IBAction private func btnOkAction(_ sender: UIButton) {
        self.objViewModel.removeSelectedTask(withCurrrentIndex: sender.tag)
        UIView.animate(withDuration: 0.2, delay: 0.2) {
            self.viewDeletePopup.alpha = 0
            self.imgBlureView.alpha = 0
            self.tblTodoList.reloadData()
        }
    }
    
    @IBAction private func btnCancelDateAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2) {
            self.viewSlecteDateHolder.isHidden = true
            self.imgBlureView.alpha = 0
        }
    }
    
    @IBAction private func btnSelectDateAction(_ sender: UIButton) {
        guard ViewModel.startDate != nil && ViewModel.endDate != nil else {
            self.showToast(message: KeyMessages.kPleaseSelectDateRange)
            return
        }
        
        UIView.animate(withDuration: 0.2) {
            self.viewSlecteDateHolder.isHidden = true
            self.imgBlureView.alpha = 0
        }
        
        ViewModel.currentFilter = .customDate
        objViewModel.filterTaskData()
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblTodoList {
            if objViewModel.hasToDoListAvailable() {
                if ViewModel.arrFilterData.count >= 1 {
                    return ViewModel.arrFilterData.count
                }else {
                    return 1
                }
            }else {
                return 1
            }
        }else if tableView == tblFilterList {
            return objViewModel.arrFilterTypes.count
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblTodoList {
            if objViewModel.hasToDoListAvailable() {
                guard let cell: NoteTableViewCell = tableView.dequeueReusableCell(withIdentifier: NoteTableViewCell.className) as? NoteTableViewCell else { return UITableViewCell()}
                cell.objTaskDetails = ViewModel.arrFilterData[indexPath.row]
                cell.btnCheckBox.tag = ViewModel.arrFilterData[indexPath.row].id ?? 0
                cell.btnCheckBox.isSelected = (ViewModel.arrFilterData[indexPath.row].isSelected ?? false)
                cell.didDeletePressed = { [weak self] tag  in
                    guard let self = self else { return }
                    self.btnOk.tag = ViewModel.arrFilterData[indexPath.row].id ?? 0
                    UIView.animate(withDuration: 0.2, delay: 0.2) {
                        self.viewDeletePopup.alpha = 1
                        self.imgBlureView.alpha = 0.8
                        self.lblDeleteMessage.text = "\(KeyMessages.kDeleteConfirmation1) \(Constant.arrToDoList[indexPath.row].taskTitle ?? Constant._NULL_STRING) \(KeyMessages.kDeleteConfirmation2)"
                    }
                }
                
                cell.reloadTableView = { [weak self] in
                    guard let self = self else { return }
                    self.tblTodoList.reloadRows(at: [indexPath], with: .automatic)
                }
                return cell
            }else {
                guard let cell: NoDataTableViewCell = tableView.dequeueReusableCell(withIdentifier: NoDataTableViewCell.className) as? NoDataTableViewCell else { return UITableViewCell()}
                return cell
            }
        }else if tableView == tblFilterList {
            guard let cell: FilterTableViewCell = tableView.dequeueReusableCell(withIdentifier: FilterTableViewCell.className) as? FilterTableViewCell else { return UITableViewCell()}
            cell.filterTypes = objViewModel.arrFilterTypes[indexPath.row]
            return cell
        }else  {
            return  UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblFilterList {
            guard (Constant.arrToDoList.count) > 0 else {
                self.viewFilterTableHolder.isHidden = true
                self.imgBlureView.alpha = 0
                return
            }
            
            ViewModel.currentFilter = objViewModel.arrFilterTypes[indexPath.row]
            
            if ViewModel.currentFilter == .customDate {
                UIView.animate(withDuration: 0.2) {
                    self.viewSlecteDateHolder.isHidden = false
                    ViewModel.startDate = nil
                    ViewModel.endDate = nil
                    self.txtStartDate.text = nil
                    self.txtEndDate.text = nil
                }
            }else {
                objViewModel.filterTaskData()
                self.imgBlureView.alpha = 0
            }
            
            self.viewFilterTableHolder.isHidden = true
        }else if tableView == tblTodoList {
            let viewAddToDoList = AddToDoViewController.instance()
            viewAddToDoList.isComeFromEdit = true
            viewAddToDoList.objTodoListModel = ViewModel.arrFilterData[indexPath.row]
            self.navigationController?.pushViewController(viewAddToDoList, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

