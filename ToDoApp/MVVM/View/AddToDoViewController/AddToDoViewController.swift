//
//  AddToDoViewController.swift
//  ToDoApp
//
//  Created by Hardik on 09/06/23.
//

import UIKit
import SkyFloatingLabelTextField
import iOSDropDown

class AddToDoViewController: BaseViewController {
    //----------------------------
    // MARK: - Outlets Declaration
    //----------------------------
    @IBOutlet private weak var txtTaskTitle: SkyFloatingLabelTextField!
    @IBOutlet private weak var txtTaskTime: SkyFloatingLabelTextField!
    @IBOutlet private weak var txtMeridiem: DropDown!
    @IBOutlet private weak var btnCancel: UIButton!
    @IBOutlet private weak var btnAdd: UIButton!
    
    //------------------------------
    // MARK: - Variables Declaration
    //------------------------------
    private let objAddToDoViewModel: AddToDoViewModel = AddToDoViewModel()
    private let timePicker = UIDatePicker()
    private var selectedTime: Date?
    var isComeFromEdit: Bool = false
    var objTodoListModel: ToDoListModel = ToDoListModel()
    
    //-------------------------------
    // MARK: - View LifeCycle Methods
    //-------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationTitle(withTitle: isComeFromEdit ? KeyMessages.kUpdateTask : KeyMessages.kAddTask, forViewController: self)
        setUpViewModelBindings()
        setupUI()
        setupPickerView()
    }
    
    //-----------------------
    // MARK: - Custom Methods
    //-----------------------
    static func instance() -> AddToDoViewController {
        _mainStoryboard.instantiateViewController(withIdentifier: AddToDoViewController.className) as! AddToDoViewController
    }

    private func setUpViewModelBindings() {
        self.objAddToDoViewModel.showAlert = { [weak self] responseMessages in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.showToast(message: responseMessages ?? "")
            }
        }
        
        self.objAddToDoViewModel.didFinishAdd = { [weak self]  in
            guard let self = self else { return }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func setupUI() {
        btnCancel.layer.cornerRadius = btnCancel.frame.height/2.0
        btnAdd.layer.cornerRadius = btnAdd.frame.height/2.0
        
        txtMeridiem.optionArray = ["AM", "PM"]
        txtMeridiem.optionIds = [1,2]

        txtMeridiem.selectedIndex = 0
        txtMeridiem.text = "AM"
        txtMeridiem.isUserInteractionEnabled = false
        
        if isComeFromEdit {
            btnAdd.setTitle(KeyMessages.kUpdateButton, for: .normal)
            objAddToDoViewModel.objCurrentData = self.objTodoListModel
            txtTaskTitle.text = objTodoListModel.taskTitle
            txtTaskTime.text = "\(objTodoListModel.taskTime ?? Date())"
            let components = Calendar.current.dateComponents([.hour, .minute], from: objTodoListModel.taskTime ?? Date())
            let hour = components.hour!
            txtMeridiem.text = hour > 12 ? "PM" : "AM"
        }
    }
    
    private func setupPickerView() {
        Formatter.time.defaultDate = Calendar.current.startOfDay(for: Date())
        timePicker.datePickerMode = .dateAndTime
        timePicker.locale = Locale(identifier: "en_US")
        timePicker.minimumDate = .now
        txtTaskTime.inputView = timePicker
        timePicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let date = timePicker.date
        let components = Calendar.current.dateComponents([.hour, .minute], from: date)
        let hour = components.hour!
        txtTaskTime.text = GetLocalStringDateFromUTC(date: date)//"\(timePicker.date)"//"\(hour):\(minute)"
        txtMeridiem.text = hour > 12 ? "PM" : "AM"
        selectedTime = date
     }
    
    //------------------
    // MARK: - IBActions
    //------------------
    @IBAction private func btnCancelAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func btnAddAction(_ sender: UIButton) {
        let title = self.txtTaskTitle.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? nil
        self.objAddToDoViewModel.validateData(taskTitle: title, taskTime: selectedTime, isComeFromUpdate: self.isComeFromEdit)
    }
}
