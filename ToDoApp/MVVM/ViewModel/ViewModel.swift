//
//  ViewModel.swift
//  ToDoApp
//
//  Created by Hardik on 09/06/23.
//

import UIKit

class ViewModel: NSObject {
    //----------------------
    // MARK: - Override init
    //----------------------
    override init(){
        
    }
    
    //------------------
    // MARK: - Variables
    //------------------
    var didFinishRemove: (() -> ())?
    var didRefreshData: (() -> ())?
    var arrFilterTypes: [FilterType] = FilterType.allCases
    static var currentFilter: FilterType = .clearFilter
    static var arrFilterData: [ToDoListModel] = []
    static var startDate: Date?
    static var endDate: Date?
    
    //-----------------------
    // MARK: - Custom Methods
    //-----------------------
    func hasToDoListAvailable() -> Bool {
        return ViewModel.arrFilterData.count > 0 ? true : false
    }
    
    func filterTaskData() {
        
        let calendar = Calendar.current
        let today = calendar.startOfDay(for: Date())
        let yesterday = calendar.date(byAdding: .day, value: -1, to: today)
        
        ViewModel.arrFilterData.removeAll()
        
        switch ViewModel.currentFilter {
        case .today:
            for i in Constant._NULL_INT..<Constant.arrToDoList.count {
                let valueDate = calendar.startOfDay(for: Constant.arrToDoList[i].taskTime ?? Date())
                if valueDate == today {
                    ViewModel.arrFilterData.append(Constant.arrToDoList[i])
                }
            }
        case .yesterday:
            for i in Constant._NULL_INT..<Constant.arrToDoList.count {
                let valueDate = calendar.startOfDay(for: Constant.arrToDoList[i].taskTime ?? Date())
                if valueDate == yesterday {
                    ViewModel.arrFilterData.append(Constant.arrToDoList[i])
                }
            }
        case .customDate:
            for i in Constant._NULL_INT..<Constant.arrToDoList.count {
                if let time = Constant.arrToDoList[i].taskTime, let startTime = ViewModel.startDate, let endTime = ViewModel.endDate {
                    if time >= startTime && time <= endTime {
                        ViewModel.arrFilterData.append(Constant.arrToDoList[i])
                    }
                }
            }
        case .sortByAZ:
            let azValues = Constant.arrToDoList.sorted { ($0.taskTitle ?? Constant._NULL_STRING) < ($1.taskTitle ?? Constant._NULL_STRING)}
            ViewModel.arrFilterData.append(contentsOf: azValues)
        case .sortByZA:
            let zaValues = Constant.arrToDoList.sorted { ($0.taskTitle ?? Constant._NULL_STRING) > ($1.taskTitle ?? Constant._NULL_STRING)}
            ViewModel.arrFilterData.append(contentsOf: zaValues)
        case .clearFilter:
            for i in Constant._NULL_INT..<Constant.arrToDoList.count {
                ViewModel.arrFilterData.append(Constant.arrToDoList[i])
            }
        }
        
        self.didRefreshData?()
    }
    
    func removeSelectedTask(withCurrrentIndex index: Int){
        if hasToDoListAvailable() {
            Constant.arrToDoList = Constant.arrToDoList.filter { $0.id != index}
            _userDefault.synchronize()
            self.didFinishRemove?()
        }
    }
}
