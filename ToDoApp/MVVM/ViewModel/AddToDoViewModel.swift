//
//  AddToDoViewModel.swift
//  ToDoApp
//
//  Created by Hardik on 09/06/23.
//

import UIKit

class AddToDoViewModel: NSObject {
    //------------------------------
    // MARK: - Variables Declaration
    //------------------------------
    var showAlert: ((String?) -> ())?
    var didFinishAdd: (() -> ())?
    private var error: String? {
        didSet { self.showAlert?(error) }
    }
    var objTaskDetails: ToDoListModel = ToDoListModel()
    var objCurrentData: ToDoListModel = ToDoListModel()
    
    //-------------------------
    // MARK: - Override Methods
    //-------------------------
    override init(){
    }
    
    //-----------------------
    // MARK: - Custom Methods
    //-----------------------
    private func addTask() {
        if Constant.arrToDoList != nil {
            Constant.arrToDoList.append(objTaskDetails)
        }else {
            Constant.arrToDoList = [objTaskDetails]
        }
        self.didFinishAdd?()
    }
    
    private func updateTask() {
        for i in Constant._NULL_INT..<Constant.arrToDoList.count {
            if Constant.arrToDoList[i].id == objCurrentData.id {
                Constant.arrToDoList[i].taskTitle = objTaskDetails.taskTitle
                Constant.arrToDoList[i].taskTime = objTaskDetails.taskTime
            }
        }
        self.didFinishAdd?()
    }
    
    func validateData(taskTitle: String?, taskTime: Date?, isComeFromUpdate: Bool = false) {
        guard taskTitle?.isEmpty != true else {
            return self.error = KeyMessages.kInsetTitle
        }
        guard taskTime != nil else {
            return self.error = KeyMessages.kInserTime
        }
        
        if isComeFromUpdate {
            objTaskDetails.id = objCurrentData.id
        }else {
            Constant.id = Constant.id + 1
            objTaskDetails.id = Constant.id
        }
        
        objTaskDetails.taskTitle = taskTitle
        objTaskDetails.taskTime = taskTime
        _print(objTaskDetails,"<=== Current todo data")
        
        isComeFromUpdate ? updateTask() : addTask()
    }
}
