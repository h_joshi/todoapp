//
//  DateExtension.swift
//  ToDoApp
//
//  Created by Hardik on 09/06/23.
//

import Foundation

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
