//
//  Formatter+Extension.swift
//  ToDoApp
//
//  Created by Hardik on 09/06/23.
//

import Foundation

extension Formatter {
    static let time: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = .init(identifier: "em_US_POSIX")
        formatter.dateFormat = "HH:mm"
        return formatter
    }()
}
