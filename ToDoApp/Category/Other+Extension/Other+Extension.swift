//
//  Other.swift
//  ToDoApp
//
//  Created by Hardik on 08/06/23.
//

import Foundation

#if os(iOS) || os(tvOS)

extension NSObject {
    public var className: String {
        return type(of: self).className
    }
    
    public static var className: String {
        return String(describing: self)
    }
}

#endif
