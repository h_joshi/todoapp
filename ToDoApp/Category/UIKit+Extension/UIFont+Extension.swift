//
//  UIFont+Extension.swift
//  ToDoApp
//
//  Created by Hardik on 09/06/23.
//

import UIKit

enum NeoTech: String {
    case roboto_medium = "Roboto-Medium"
    case roboto_regular = "Roboto-Regular"
    case rubik_regular = "Rubik-Regular"
}

extension UIFont{
    class func robotoFontWith(fontType: NeoTech, size:CGFloat) -> UIFont{
        return UIFont(name: fontType.rawValue, size: size)!
    }
}

extension UIFont {
    //print out all the fonts
    func printAllFonts(){
        for family: String in UIFont.familyNames {
               print("\(family)")
               for names: String in UIFont.fontNames(forFamilyName: family) {
                   print("== \(names)")
               }
           }
    }
}
