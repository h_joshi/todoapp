//
//  NoteTableViewCell.swift
//  ToDoApp
//
//  Created by Hardik on 08/06/23.
//

import UIKit

class NoteTableViewCell: UITableViewCell {
    //----------------------------
    // MARK: - Outlets Declaration
    //----------------------------
    @IBOutlet weak var lblTaskTitle: UILabel!
    @IBOutlet private weak var lblTaskTime: UILabel!
    @IBOutlet weak var lblTaskStatus: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet private weak var btnClose: UIButton!
    
    //------------------
    // MARK: - Variables
    //------------------
    var objTaskDetails: ToDoListModel? {
        willSet{
            lblTaskTitle.attributedText = lblTaskTitle.text?.removeStrikeThrough()
        }
        didSet {
            btnClose.tag = objTaskDetails?.id ?? Constant._NULL_INT
            lblTaskTitle.text = objTaskDetails?.taskTitle
            
            if objTaskDetails?.isSelected == true {
                let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: lblTaskTitle.text ?? Constant._NULL_STRING)
                    attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: 0, length: attributeString.length))
                attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.blackColor, range: NSRange(location: 0, length: attributeString.length))
                lblTaskTitle.attributedText = attributeString
                lblTaskStatus.isHidden = true
            }else {
                lblTaskTitle.attributedText = highlightedIfOverdue(text: (objTaskDetails?.taskTitle ?? Constant._NULL_STRING), timestamp: objTaskDetails?.taskTime ?? Date())
                lblTaskStatus.isHidden = Date() > objTaskDetails?.taskTime ?? Date() ? false : true
            }
            
            lblTaskTime.text = GetLocalStringDateFromUTC(date: objTaskDetails?.taskTime ?? Date())
        }
    }
    
    var didDeletePressed: ((Int?) -> ())?
    var reloadTableView: (() -> ())?
   
    //-------------------------
    // MARK: - Required Methods
    //-------------------------
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //-----------------------
    // MARK: - Custom Methods
    //-----------------------
    
    func highlightedIfOverdue(text: String, timestamp: Date) -> NSAttributedString {
        let currentDate = Date()
        let attributes: [NSAttributedString.Key: Any]
        attributes = [.foregroundColor: currentDate > timestamp ? AppColor.redColor : AppColor.blackColor]
        
        let attributedText = NSAttributedString(string: lblTaskTitle.text ?? Constant._NULL_STRING, attributes: attributes)
        return attributedText
    }
    
    func getHoursAndMinutes(timeStamp: Date) -> (hours: Int, minutes: Int) {
        let calendar = Calendar.current
        let components = calendar.dateComponents ( [.hour, .minute], from: timeStamp)
        let hours = components.hour ?? Constant._NULL_INT
        let minutes = components.minute ?? Constant._NULL_INT
        
        return(hours,minutes)
    }
    
    func GetLocalStringDateFromUTC(date:Date)-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+5:30") //Current time zone different
        let myString = dateFormatter.string(from: date) //according to date format your date string
        return myString //Convert String to Date
    }
    
    //------------------
    // MARK: - IBActions
    //------------------
    @IBAction private func btnCheckBoxAction(_ sender: UIButton) {
        sender.isSelected.toggle()
        if sender.isSelected {
            for i in Constant._NULL_INT..<Constant.arrToDoList.count {
                if Constant.arrToDoList[i].id == sender.tag {
                    Constant.arrToDoList[i].isSelected = true
                }
            }
            
            for i in Constant._NULL_INT..<ViewModel.arrFilterData.count {
                if ViewModel.arrFilterData[i].id == sender.tag {
                    ViewModel.arrFilterData[i].isSelected = true
                }
            }
            
            _userDefault.synchronize()
            lblTaskTitle.attributedText = lblTaskTitle.text?.addStrikeThrough()
        }else {
            for i in Constant._NULL_INT..<Constant.arrToDoList.count {
                if Constant.arrToDoList[i].id == sender.tag {
                    Constant.arrToDoList[i].isSelected = false
                }
            }
            
            for i in Constant._NULL_INT..<ViewModel.arrFilterData.count {
                if ViewModel.arrFilterData[i].id == sender.tag {
                    ViewModel.arrFilterData[i].isSelected = false
                }
            }
            _userDefault.synchronize()
            lblTaskTitle.attributedText = lblTaskTitle.text?.removeStrikeThrough()
            lblTaskTitle.attributedText = highlightedIfOverdue(text: (objTaskDetails?.taskTitle ?? Constant._NULL_STRING), timestamp: objTaskDetails?.taskTime ?? Date())
        }
        
        _print(Constant.arrToDoList)
        reloadTableView?()
    }
    
    @IBAction private func btnCloseAction(_ sender: UIButton) {
        self.didDeletePressed?(sender.tag)
    }
    
}

extension String {
    func addStrikeThrough() -> NSAttributedString {
        let attributeString = NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle,
                                     value: 2,
                                     range: NSRange(location: 0, length: attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.strikethroughColor,
                                     value: AppColor.blackColor,
                                     range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: AppColor.blackColor, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
    func removeStrikeThrough() -> NSAttributedString {
        let attributeString = NSMutableAttributedString(string: self)
        attributeString.removeAttribute(NSAttributedString.Key.strikethroughStyle, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
}

