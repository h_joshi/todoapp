//
//  NoDataTableViewCell.swift
//  ToDoApp
//
//  Created by Hardik on 08/06/23.
//

import UIKit

class NoDataTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
