//
//  FilterTableViewCell.swift
//  ToDoApp
//
//  Created by Hardik on 12/06/23.
//

import UIKit

class FilterTableViewCell: UITableViewCell {
    //----------------------------
    // MARK: - Outlets Declaration
    //----------------------------
    @IBOutlet private weak var lblFilterType: UILabel!
    
    //------------------------------
    // MARK: - Variables Declaration
    //------------------------------
    var filterTypes: FilterType? {
        didSet {
            lblFilterType.text = filterTypes?.rawValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
